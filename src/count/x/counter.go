package x

import (
	"circuit/use/circuit"
	"time"
)

type Counter struct {
	val int
}

func init() {
	circuit.RegisterValue(&Counter{})
}

func (c *Counter) Inc() {
	c.val += 1
}

func (c *Counter) Dec() {
	c.val -= 1
}

func (c *Counter) Val() int {
	return c.val
}

type StartCounter struct{}

func (StartCounter) Main() int {
	cnt := &Counter{}
	circuit.Listen("counter-service", cnt)
	circuit.RunInBack(func () { time.Sleep(20*time.Second) } )
	return 1
}

func init() {
	circuit.RegisterFunc(StartCounter{})
}
