package main

import (
	_ "circuit/load/cmd"
	"circuit/use/circuit"
	"count/x"
	"fmt"
	"log"
)

func main() {
	fmt.Println("starting...")
	ret, addr, err := circuit.Spawn("54.84.22.156", []string{"/count"}, x.StartCounter{})
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("spawned on", addr.String())
	fmt.Println("returned ", ret)
	
	fmt.Println("dialing...")
	cnt := circuit.Dial(addr, "counter-service")
	fmt.Println("dialed.")

	for i := 0; i < 10; i++ {
		ret := cnt.Call("Val")
		val := ret[0].(int)
		fmt.Println("val:", val)
		cnt.Call("Inc")
	}
	
}
